from ._loaders import airfoil, boston, california_housing, concrete, dow_chemical, \
    energy_cooling, energy_heating, nguyen, tower, wine_red, wine_white, \
    yacht_hydrodynamics

__all__ = [
    "airfoil",
    "boston",
    "california_housing",
    "concrete",
    "dow_chemical",
    "energy_cooling",
    "energy_heating",
    "nguyen",
    "tower",
    "wine_red",
    "wine_white",
    "yacht_hydrodynamics"
]
